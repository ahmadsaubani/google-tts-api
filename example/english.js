"use strict";

var googleTTS = require('..');

var test = 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Culpa facilis exercitationem laborum odio, voluptates quasi rem assumenda alias soluta ullam ab velit, similique quo tempore! Maiores odit fugit at dolore ut perferendis doloribus ipsum possimus quaerat reprehenderit error ullam quidem quam, laborum, velit sapiente provident aperiam neque quo veritatis qui?';
// var test = 'coba aja';
// var test = 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Deleniti, nam debitis error laudantium, non at dignissimos, optio reprehenderit adipisci sequi labore eum harum assumenda nisi sapiente praesentium laboriosam facilis. Ipsam, repellat eius. Aliquid similique illum quidem eaque fuga fugiat optio alias, rem consequatur quod ut non placeat praesentium provident debitis consectetur necessitatibus ducimus ratione officia impedit repudiandae. Assumenda aperiam, sequi facilis porro voluptas quis harum officiis tempore autem odit minus veniam delectus doloremque. Accusamus rerum fugiat officiis nisi iure nesciunt ratione molestias ducimus voluptate dolorum odio blanditiis, consequatur quasi qui voluptatibus fugit. Similique incidunt, excepturi quidem nam reprehenderit voluptate quia?';
googleTTS(test, 'id', 1)   // speed normal = 1 (default), slow = 0.24
.then(function (url) {
  console.log(url); // https://translate.google.com/translate_tts?...
})
.catch(function (err) {
  console.error(err.stack);
});